@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">Create Crops</div>
        <div class="card-body">
            <form action="/crop/savecrops" method="post">
                @csrf
                <input type="text" name="crops" class="form-control" placeholder="Enter Crops"><br>
                <input type="submit" value="Add" class="btn btn-primary">
            </form>
        </div>
    </div>
</div>
</div>
<div class="container">
    <table class="table">
        <thead>
            <tr>
                <th scope="col">Avialable crops</th>
            </tr>
        </thead>
        <tbody>
            @foreach($crops as $crop)
            <tr>
                <td>
                    {{$crop->name}}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection