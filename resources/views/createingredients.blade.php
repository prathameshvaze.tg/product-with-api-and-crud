@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">Create Ingredients</div>
        <div class="card-body">
            <form action="/ingredients/saveingredients" method="post" enctype="multipart/form-data">
                @csrf
                <input type="text" name="ingredients" class="form-control" placeholder="Enter Ingrendients"><br>
                
                <div class="form-group">
                    <label for="ingredientimage">Ingredients Image</label>
                    <input type="file" class="form-control-file" id="ingredientimage" name="ingredientimage">
                </div>
                <input type="submit" value="Add" class="btn btn-primary">
            </form>
        </div>
    </div>
</div>
</div>
<div class="container">
    <table class="table">
        <thead>
            <tr>
                <th scope="col">Avialable crops</th>
            </tr>
        </thead>
        <tbody>
            @foreach($ingredients as $i)
            <tr>
                <td>
                    {{$i->name}}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection