@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">Create Diseases</div>
        <div class="card-body">
            <form action="/disease/savediseases" method="post">
                @csrf
                <input type="text" name="diseases" class="form-control" placeholder="Enter Diseases"><br>
                <input type="submit" value="Add" class="btn btn-primary">
            </form>
        </div>
    </div>
</div>
</div>
<div class="container">
    <table class="table">
        <thead>
            <tr>
                <th scope="col">Avialable Diseases</th>
            </tr>
        </thead>
        <tbody>
            @foreach($diseases as $disease)
            <tr>
                <td>
                    {{$disease->name}}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection