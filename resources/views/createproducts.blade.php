@extends('layouts.app')

@section('content')
<div class="container">
</div>
<div class="container">
    <form action="/product/saveproducts" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" id="name" placeholder="Enter Product Name" name="products">
        </div>

        <div class="form-group">
            <label for="producttype">Select Product Type</label>
            <select class="form-select" aria-label="Select Ingredients" name="producttype">
                <option selected>Select Product Type</option>
                @foreach($types as $type)
                <option value="{{ $type->id }}">{{$type->name}}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="ingredients">Select Product Ingredients</label>
            <select class="form-select" aria-label="Select Ingredients" name="ingredients[]" multiple>
                <option selected>Select ingredients</option>
                @foreach($ingredients as $i)
                <option value="{{ $i->id }}">{{$i->name}}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="producttype">Select Product Crops</label>
            <select class="form-select" aria-label="Select Ingredients" name="crop[]" multiple>
                <option selected>Select Product Crops</option>
                @foreach($crops as $crop)
                <option value="{{ $crop->id }}">{{$crop->name}}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="producttype">Select Product Disease</label>
            <select class="form-select" aria-label="Select Disease" name="disease[]" multiple>
                <option selected>Select Product Disease</option>
                @foreach($diseases as $disease)
                <option value="{{ $disease->id }}">{{$disease->name}}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="productimage">Product Image</label>
            <input type="file" class="form-control-file" id="productimage" name="productimage">
        </div>


        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
@endsection