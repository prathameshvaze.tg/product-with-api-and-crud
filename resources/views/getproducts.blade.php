@extends('layouts.app')

@section('content')
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
<!-- <link href="{{ asset('/css/allnews.css') }}" rel="stylesheet"> -->
<div class="container">
    <H1>Product details</H1>
    <table class="table">
        <thead>
            <th>Sr. No.</th>
            <th>Product Name</th>
            <th>Product Type</th>
            <th>Product Ingredients</th>
            <th>Product Disease</th>
            <th>Product Crops</th>
            <th>Delete Product</th>
        </thead>
        <tbody>
            @foreach($product as $p)
            <tr>
                <td>
                    {{$p->id}}
                </td>
                <td>
                    {{$p->name}}
                </td>
                <td>
                    {{$p->type->name}}
                </td>
                <td>
                    @foreach($p->ingredient as $i)
                    <a href="/ingredients/{{$i->id}}" class="link">
                        {{$i->name}}
                    </a>

                    @endforeach
                </td>
                <td>
                    @foreach($p->disease as $d)
                    <a href="/disease/{{$d->id}}" class="link">
                        {{$d->name}}
                    </a>

                    @endforeach
                </td>
                <td>
                    @foreach($p->crop as $c)
                    <a href="/crop/{{$c->id}}" class="link">
                        {{$c->name}}
                    </a>

                    @endforeach
                </td>
                <td>
                    <a href="/product/del/{{$p->id}}" class="link">
                        Delete
                    </a>
                </td>

            </tr>
            @endforeach
        </tbody>
    </table>

    <div class="row">
        <div class="col-12 d-flex justify-content-end">
            {{ $product->links() }}
        </div>
    </div>
</div>
@endsection