<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Crop extends Model
{
    protected $fillable = [
        'name'
    ];

    public function product()
    {
        return $this->belongsToMany(Product::class, 'product_crop_pivots');
    }
}
