<?php

namespace App\Http\Controllers;

use App\Disease;
use Illuminate\Http\Request;

class DiseaseController extends Controller
{
    public function index()
    {
        $diseases = Disease::get();
        return view('createdisease', compact('diseases'));
    }

    public function show($id)
    {
        $disease = Disease::findOrFail($id);
        return view('getproducts', [
            'products' => $disease->product
        ]);
    }

    public function store(Request $request)
    {
        $input['name'] = $request->diseases;
        Disease::create($input);
        return redirect('/disease');
    }
}
