<?php

namespace App\Http\Controllers;

use App\Crop;
use Illuminate\Http\Request;

class CropController extends Controller
{
    public function index()
    {
        $crops = Crop::get();
        return view('createcrop', compact('crops'));
    }

    public function show($id)
    {
        $crop = Crop::findOrFail($id);
        return view('getproducts', [
            'products' => $crop->product
        ]);
    }

    public function store(Request $request)
    {
        $input['name'] = $request->crops;
        Crop::create($input);
        return redirect('/crop');
    }
}
