<?php

namespace App\Http\Controllers;

use App\Crop;
use App\Disease;
use App\Ingredients;
use App\Transformers\ProductTransformer;
use App\Transformers\IngredientsTransformer;
use App\Transformers\DiseaseTransformer;
use App\Transformers\CropTransformer;
use App\Transformers\ProductDetailsTransformer;
use App\Transformers\ProductTypeTransformer;
use App\Product;
use App\Types;
use Illuminate\Http\Request;

class ProductAPIController extends Controller
{
    public function getproducts()
    {
        $productlist = Product::all()->transformWith(new ProductTransformer())->toJson();
        return $productlist;
    }

    public function getingredients()
    {
        $ingredientlist = Ingredients::all()->transformWith(new IngredientsTransformer())->toJson();
        return $ingredientlist;
    }

    public function getproducttypes()
    {
        $typelist = Types::all()->transformWith(new ProductTypeTransformer())->toJson();
        return $typelist;
    }

    public function getcrops()
    {
        $croplist = Crop::all()->transformWith(new CropTransformer())->toJson();
        return $croplist;
    }

    public function getdiseases()
    {
        $diseaselist = Disease::all()->transformWith(new DiseaseTransformer())->toJson();
        return $diseaselist;
    }

    public function getproductdetails()
    {
        // $productdetaillist = Product::all()->transformWith(new ProductDetailsTransformer())->toJson();
        // return $productdetaillist;
        $product = Product::all();
        return fractal()
            ->collection($product)
            ->parseIncludes('crop', 'disease','ingredient')
            ->transformWith(new ProductDetailsTransformer())
            ->toJson();
    }
}
