<?php

namespace App\Http\Controllers;

use App\Ingredients;
use Illuminate\Http\Request;

class IngredientsController extends Controller
{

    public function index()
    {
        $ingredients = Ingredients::get();
        return view('createingredients', compact('ingredients'));
    }

    public function show($id)
    {
        $ingredients = Ingredients::findOrFail($id);
        return view('getproducts', [
            'products' => $ingredients->product
        ]);
    }

    public function store(Request $request)
    {
        $filepath = $request->file('ingredientimage')->store('Ingredients');
        // dd($filepath);
        $input1['name'] = $request->ingredients;
        $input1['image'] = $filepath;
        Ingredients::create($input1);
        return redirect('/ingredients');
    }
}
