<?php

namespace App\Http\Controllers;

use App\Crop;
use App\Disease;
use App\Ingredients;
use App\Product;
use App\Types;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        $product = Product::with('crop', 'disease', 'ingredient')
            ->paginate(9);
        return view('getproducts', compact('product'));
    }

    public function show()
    {
    }

    public function store(Request $request)
    {
        $filepath = $request->file('productimage')->store('Products');
        $input1['name'] = $request->products;
        $input1['image'] = $filepath;
        $input1['type_id'] = $request->producttype;
        $product = Product::create($input1);

        $product->crop()->attach($request->crop);
        $product->save();

        $product->disease()->attach($request->disease);
        $product->save();

        $product->ingredient()->attach($request->ingredients);
        $product->save();

        return redirect('/product');
    }

    public function create()
    {
        $types = Types::get();
        $crops = Crop::get();
        $diseases = Disease::get();
        $ingredients = Ingredients::get();
        return view('createproducts', compact('types', 'diseases', 'crops', 'ingredients'));
    }

    public function destroy($id)
    {
        $product = Product::find($id);
        $product->delete();
        return redirect('/product');
    }
}
