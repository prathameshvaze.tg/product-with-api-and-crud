<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class APICrudController extends Controller
{
    public function create(Request $request)
    {
        $products = new Product; 

        $products->name = $request->input('name');
        $products->image = $request->input('image');
        $products->type_id = $request->input('type_id');

        $products->save();
        return response()->json($products);
    }

    public function show()
    {
        $products = Product::all();
        return response()->json($products);
    }

    public function showbyid($id)
    {
        $products = Product::find($id);
        return response()->json($products);
    }

    public function updatebyid(Request $request,$id)
    {
        $products = Product::find($id);
        $products->name = $request->input('name');
        $products->image = $request->input('image');
        $products->type_id = $request->input('type_id');
        $products->save();

        return response()->json($products);
    }

    public function deletebyid(Request $request,$id)
    {
        $products = Product::find($id);
        $products->delete();
        return response()->json($products);
    }
}
