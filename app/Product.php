<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $fillable = [
        'name',
        'image',
        'type_id'
    ];

    public function type()
    {
        return $this->hasOne(Types::class,'id','type_id');
    }

    public function crop()
    {
        return $this->belongsToMany(Crop::class,'product_crop_pivots')->withTimestamps();
    }

    public function disease()
    {
        return $this->belongsToMany(Disease::class,'product_disease_pivots')->withTimestamps();
    }

    public function ingredient()
    {
        return $this->belongsToMany(Ingredients::class,'product_ingredients_pivots')->withTimestamps();
    }
}
