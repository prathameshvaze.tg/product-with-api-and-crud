<?php

namespace App\Transformers;

use App\Crop;
use League\Fractal\TransformerAbstract;

class CropTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    // protected $defaultIncludes = [
    //     //
    // ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    // protected $availableIncludes = [
    //     //
    // ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Crop $crop)
    {
        return [
            'crop-id' => $crop->id,
            'crop-name' => $crop->name,
            'crop-deleted-at' => $crop->deleted_at,
            'crop-created-at' => $crop->created_at,
            'crop-updated-at' => $crop->updated_at,
        ];
    }
}
