<?php

namespace App\Transformers;

use App\Product;
use League\Fractal\TransformerAbstract;

class ProductTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    // protected $defaultIncludes = [
    //     //
    // ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    // protected $availableIncludes = [
    //     //
    // ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Product $product)
    {
        return [
            'product-id' => $product->id,
            'product-name' => $product->name,
            'product-image-path' => $product->image,
            'product-image-path' => $product->type_id,
            'product-deleted-at' => $product->deleted_at,
            'product-created-at' => $product->created_at,
            'product-updated-at' => $product->updated_at,
        ];
    }
}
