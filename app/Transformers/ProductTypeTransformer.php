<?php

namespace App\Transformers;

use App\Types;
use League\Fractal\TransformerAbstract;

class ProductTypeTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    // protected $defaultIncludes = [
    //     //
    // ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    // protected $availableIncludes = [
    //     //
    // ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Types $types)
    {
        return [
            'types-id' => $types->id,
            'types-name' => $types->name,
            'types-deleted-at' => $types->deleted_at,
            'types-created-at' => $types->created_at,
            'types-updated-at' => $types->updated_at,
        ];
    }
}
