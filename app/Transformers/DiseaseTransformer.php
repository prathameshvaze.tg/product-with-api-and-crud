<?php

namespace App\Transformers;

use App\Disease;
use League\Fractal\TransformerAbstract;

class DiseaseTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    // protected $defaultIncludes = [
    //     //
    // ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    // protected $availableIncludes = [
    //     //
    // ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Disease $disease)
    {
        return [
            'disease-id' => $disease->id,
            'disease-name' => $disease->name,
            'disease-deleted-at' => $disease->deleted_at,
            'disease-created-at' => $disease->created_at,
            'disease-updated-at' => $disease->updated_at,
        ];
    }
}
