<?php

namespace App\Transformers;

use App\Product;
use League\Fractal\TransformerAbstract;

class ProductDetailsTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    // protected $defaultIncludes = [
    //     //
    // ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'crop',
        'disease',
        'ingredient'
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Product $product)
    {
        return [
            'product-id' => $product->id,
            'product-name' => $product->name,
            'product-image-path' => $product->image,
            'product-image-path' => $product->type_id,
            'product-deleted-at' => $product->deleted_at,
            'product-created-at' => $product->created_at,
            'product-updated-at' => $product->updated_at,
        ];
    }

    public function includeDisease(Product $product)
    {
        $disease = $product->disease;
        return $this->collection($disease, new DiseaseTransformer);
    }

    public function includeCrop(Product $product)
    {
        $crop = $product->crop;
        return $this->collection($crop, new CropTransformer);
    }

    public function includeIngredient(Product $product)
    {
        $ingredient = $product->ingredient;
        return $this->collection($ingredient, new IngredientsTransformer);
    }
}
