<?php

namespace App\Transformers;

use App\Ingredients;
use League\Fractal\TransformerAbstract;

class IngredientsTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    // protected $defaultIncludes = [
    //     //
    // ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    // protected $availableIncludes = [
    //     //
    // ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Ingredients $ingredient)
    {
        return [
            'ingredient-id' => $ingredient->id,
            'ingredient-name' => $ingredient->name,
            'ingredient-deleted-at' => $ingredient->deleted_at,
            'ingredient-created-at' => $ingredient->created_at,
            'ingredient-updated-at' => $ingredient->updated_at,
        ];
    }
}
