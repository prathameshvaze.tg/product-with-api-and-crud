<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1'], function () {
    Route::post('/product', 'APICrudController@create');
    Route::get('/product', 'APICrudController@show');
    Route::get('/product/{id}', 'APICrudController@showbyid');
    Route::put('/product/{id}', 'APICrudController@updatebyid');
    Route::delete('/product/{id}', 'APICrudController@deletebyid');
});