<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Types;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'product'], function () {
    Route::get('/', 'ProductController@index')->name('product.view');
    Route::get('/addproducts', 'ProductController@create')->name('product.addproducts');
    Route::get('/{id}', 'ProductController@show')->name('product.viewbyid');
    Route::post('/saveproducts', 'ProductController@store')->name('product.create');
    Route::get('del/{id}', 'ProductController@destroy')->name('product.delete');
});

Route::group(['prefix' => 'ingredients'], function () {
    Route::get('/', 'IngredientsController@index')->name('ingredients.view');
    Route::get('/{ingredients}', 'IngredientsController@show')->name('ingredients.viewbyid');
    Route::post('/saveingredients', 'IngredientsController@store')->name('ingredients.create');
});

Route::group(['prefix' => 'crop'], function () {
    Route::get('/', 'CropController@index')->name('crop.view');
    Route::get('/{crop}', 'CropController@show')->name('crop.viewbyid');
    Route::post('/savecrops', 'CropController@store')->name('crop.create');
});

Route::group(['prefix' => 'disease'], function () {
    Route::get('/', 'DiseaseController@index')->name('disease.view');
    Route::get('/{disease}', 'DiseaseController@show')->name('disease.viewbyid');
    Route::post('/savediseases', 'DiseaseController@store')->name('disease.create');
});

Route::group(['prefix' => 'api/v1'], function () {
    Route::get('/getproducts', 'ProductAPIController@getproducts');
    Route::get('/getingredients', 'ProductAPIController@getingredients');
    Route::get('/getproducttypes', 'ProductAPIController@getproducttypes');
    Route::get('/getcrops', 'ProductAPIController@getcrops');
    Route::get('/getdiseases', 'ProductAPIController@getdiseases');
    Route::get('/getproductdetails', 'ProductAPIController@getproductdetails');
});

// Route::resource('api/v1/product', 'APICrudController');

Route::get('/type/{author}', function (Types $type) {
    return view('getproducts', [
        'product' => $type->product
    ]);
});
